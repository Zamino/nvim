" elixir.vim

let currnet_compiler = 'elixir'
CompilerSet makeprg=elixir\ %
CompilerSet errorformat=**\ (%.%#)\ %f:%l:%m
