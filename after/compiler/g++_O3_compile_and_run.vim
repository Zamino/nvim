" g++_O3_compile_and_run.vim

let currnet_compiler = 'g++_O3_compile_and_run'
CompilerSet makeprg=g++\ -std=c++23\ -g\ -O3\ -o\ '%:p:r.bin'\ '%'\ &&\ '%:p:r.bin'

"CompilerSet errorformat=%t:%f:%l:%m
