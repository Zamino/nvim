" pandoc_markdown_pdf.vim

let currnet_compiler = 'pandoc_markdown_pdf'
CompilerSet makeprg=pandoc\ --pdf-engine=xelatex\ --self-contained\ -f\ markdown\ -t\ latex\ -o\ '%:p:r.pdf'\ '%'
CompilerSet errorformat=%f:%l:%m
