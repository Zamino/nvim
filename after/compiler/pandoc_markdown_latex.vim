" tex.vim

let currnet_compiler = 'pandoc_markdown_latex'
CompilerSet makeprg=pandoc\ --self-contained\ -f\ markdown\ -t\ latex\ -o\ '%:p:r.latex'\ '%'
CompilerSet errorformat=%f:%l:%m
