" pandoc_markdown_slides.vim

let currnet_compiler = 'pandoc_markdown_slides'
"CompilerSet makeprg=pandoc\ -f\ markdown\ -t\ beamer\ -o\ '%:p:r.pdf'\ '%'
CompilerSet makeprg=pandoc\ -f\ markdown\ -t\ beamer\ --template\ ~/.local/share/beamer-themes/beamer/themes/Execushares.tex\ -s\ -o\ '%:p:r.pdf'\ '%'
CompilerSet errorformat=%f:%l:%m
