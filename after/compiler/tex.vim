" tex.vim

let currnet_compiler = 'tex'
"CompilerSet makeprg=xelatex\ '%'
CompilerSet makeprg=pdflatex\ -shell-escape\ '%'
CompilerSet errorformat=%f:%l:%m
