" g++_warning.vim

let currnet_compiler = 'g++_warning'
CompilerSet makeprg=g++\ -Wall\ -Werror\ -Wextra\ -Wpedantic\ -g\ -O0\ -o\ '%:p:r.bin'\ '%'

"CompilerSet errorformat=%t:%f:%l:%m
