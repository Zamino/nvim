" pandoc_markdown_slides.vim

let currnet_compiler = 'pandoc_markdown_slides'
CompilerSet makeprg=pandoc\ --latex-engine=xelatex\ -f\ markdown\ -t\ latex\ --template\ ~/.local/share/latex_templates/letter_template.tex\ -s\ -o\ '%:p:r.pdf'\ '%'
CompilerSet errorformat=%f:%l:%m
