" g++_compile_and_run.vim

let currnet_compiler = 'g++_compile_and_run'
CompilerSet makeprg=g++\ -Wall\ -Werror\ -Wextra\ -Wpedantic\ -std=c++23\ -g\ -O0\ -o\ '%:p:r.bin'\ '%'\ &&\ '%:p:r.bin'

"CompilerSet errorformat=%t:%f:%l:%m
