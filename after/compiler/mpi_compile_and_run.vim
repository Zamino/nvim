" mpi_compile_and_run.vim

let currnet_compiler = 'mpi_compile_and_run'
CompilerSet makeprg=mpicxx\ -Wall\ -Werror\ -Wextra\ -Wpedantic\ -std=c++23\ -g\ -O0\ -o\ '%:p:r.bin'\ '%'\ &&\mpirun\ -np\ 2\ '%:p:r.bin'

"CompilerSet errorformat=%t:%f:%l:%m
