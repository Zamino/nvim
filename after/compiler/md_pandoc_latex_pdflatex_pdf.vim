" md_pandoc_latex_pdflatex_pdf.vim

let currnet_compiler = 'md_pandoc_latex_pdflatex_pdf'
CompilerSet makeprg=pandoc\ --self-contained\ -f\ markdown\ -t\ latex\ -o\ '%:p:r.latex'\ '%'\ &&\ pdflatex\ '%:p:r.latex'
CompilerSet errorformat=%f:%l:%m
