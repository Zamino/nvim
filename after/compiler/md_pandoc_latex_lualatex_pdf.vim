" md_pandoc_latex_lualatex_pdf.vim

let currnet_compiler = 'md_pandoc_latex_lualatex_pdf'
CompilerSet makeprg=pandoc\ --self-contained\ -f\ markdown\ -t\ latex\ -o\ '%:p:r.latex'\ '%'\ &&\ lualatex\ '%:p:r.latex'
CompilerSet errorformat=%f:%l:%m
