" pandoc_markdown_slides.vim

let currnet_compiler = 'pandoc_markdown_slides'
" CompilerSet makeprg=pandoc\ -f\ markdown\ -t\ beamer\ --template\ ~/.local/share/beamer-themes/beamer/themes/Execushares.tex\ -s\ -o\ '%:p:r.tex'\ '%'
CompilerSet makeprg=pandoc\ --pdf-engine=lualatex\ -f\ markdown\ -t\ beamer\ -s\ -o\ '%:p:r.tex'\ '%'
CompilerSet errorformat=%f:%l:%m


