" g++_O3.vim

let currnet_compiler = 'g++_O3'
CompilerSet makeprg=g++\ -std=c++23\ -ggdb\ -Ofast\ -march=native\ -o\ '%:p:r.bin'\ '%'

"CompilerSet errorformat=%t:%f:%l:%m
