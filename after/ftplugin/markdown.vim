" markdown.vim

"compiler pandoc_markdown_pdf
compiler md_pandoc_latex_pdflatex_pdf
setlocal autowrite

set tabstop=4
set shiftwidth=4
