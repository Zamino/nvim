" tex.vim

" make {{{

setlocal ft=tex
setlocal makeprg=pdflatex\ -output-directory\ '%:p:h'\ -file-line-error\ %
setlocal errorformat=%f:%l:%m
setlocal autowrite

" }}}
