" pl.vim

" make {{{

setlocal makeprg=~/.config/nvim/file_extentions/vimparse.pl\ -I/opt/kivitendo-erp/\ -c\ %\ $*
setlocal errorformat=%t:%f:%l:%m

" }}}
