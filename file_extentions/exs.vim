" exs.vim

" make {{{

setlocal makeprg=elixir\ %
setlocal errorformat=**\ (%.%#)\ %f:%l:%m
setlocal autowrite

" }}}
