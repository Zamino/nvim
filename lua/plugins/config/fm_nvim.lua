-- fm_nvim.lua

require('fm-nvim').setup{
  -- Border around floating window
  border   = "shadow", -- opts: 'rounded'; 'double'; 'single'; 'solid'; 'shawdow'

  -- Percentage (0.8 = 80%)
  height   = 0.9,
  width    = 0.9,

  -- Command used to open files
  edit_cmd = "edit", -- opts: 'tabedit'; 'split'; 'pedit'; etc...

  -- Terminal commands used w/ file manager
  cmds = {
    lf_cmd      = "lf", -- eg: lf_cmd = "lf -command 'set hidden'"
    fm_cmd      = "fm",
    nnn_cmd     = "nnn",
    fff_cmd     = "fff",
    twf_cmd     = "twf",
    fzf_cmd     = "fzf", -- eg: fzf_cmd = "fzf --preview 'bat --style=numbers --color=always --line-range :500 {}'",
    fzy_cmd     = "find . | fzy",
    xplr_cmd    = "xplr",
    vifm_cmd    = "vifm",
    skim_cmd    = "sk",
    broot_cmd   = "broot",
    ranger_cmd  = "ranger",
    joshuto_cmd = "joshuto",
  },

  -- Mappings used inside the floating window
  mappings = {
    vert_split = "<C-v>",
    horz_split = "<C-h>",
    tabedit    = "<C-t>",
    edit       = "<C-e>",
    ESC        = "<ESC>"
  }
}

local map = require('helper.key_map')
map('n', '<Leader>fm', ':Vifm<Enter>', 'noremap', 'silent')
