-- cmp.lua

local cmp = require'cmp'
-- helper functions {{{
local function select_next_or_complete(fallback)
  if #cmp.core:get_sources() > 0 and not require('cmp.config').get().experimental.native_menu then
    if cmp.visible() then
      cmp.select_next_item()
    else
      cmp.complete()
    end
  else
    fallback()
  end
end

local function confirm_or_enter(fallback)
  if #cmp.core:get_sources() > 0 and not require('cmp.config').get().experimental.native_menu then
    if cmp.visible() then
      cmp.select_next_item()
    else
      cmp.complete()
    end
  else
    fallback()
  end
end
-- }}}

-- load sources with autocmd {{{
local sources = {
    { name = 'nvim_lsp', max_item_count = 10, priority = 900 },
    { name = 'luasnip', priority = 1000 },
    --{ name = 'treesitter' },
    { name = 'buffer', max_item_count = 10, priority = 700 },
    { name = 'calc' , priority = 800},
    --{ name = 'latex_symbols' },
    --{ name = 'nvim_lua' },
    { name = 'rg', keyword_length = 3, max_item_count = 5, priority = 100 },
    { name = 'path', priority = 600 },
    --{ name = 'emoji' },
    --{ name = 'spell' },
}

local autocmd = require('autocmd')

autocmd.FileType = {
  'lua',
  function()
    local l_sources = sources
    table.insert(l_sources, {name = 'nvim_lua', max_item_count = 10, priority = 700})
    require'cmp'.setup.buffer {
      sources = l_sources,
    }
  end,
}
autocmd.FileType = {
  'markdown',
  function()
    local l_sources = sources
    table.insert(l_sources, {name = 'spell', max_item_count = 5, priority = 300})
    table.insert(l_sources, {name = 'emoji', max_item_count = 10, priority = 300})
    --table.insert(l_sources, {name = 'latex_symbols', max_item_count = 5, priority = 300})
    require'cmp'.setup.buffer {
      sources = l_sources,
    }
  end,
}
autocmd.FileType = {
  'tex',
  function()
    local l_sources = sources
    table.insert(l_sources, {name = 'spell', max_item_count = 5, priority = 300})
    --table.insert(l_sources, {name = 'latex_symbols', max_item_count = 5, priority = 300})
    require'cmp'.setup.buffer {
      sources = l_sources,
    }
  end,
}
autocmd.FileType = {
  'gitcommit',
  function()
    local l_sources = sources
    table.insert(l_sources, {name = 'spell', max_item_count = 10, priority = 300})
    table.insert(l_sources, {name = 'emoji', max_item_count = 10, priority = 300})
    table.insert(l_sources, {name = 'git', priority = 800})
    require'cmp'.setup.buffer {
      sources = l_sources,
    }
  end,
}
autocmd.FileType = {
  {'sh', 'zsh', 'bash'},
  function()
    local l_sources = sources
    table.insert(l_sources, {name = 'buffer', keyword_length = 3, max_item_count = 5, priority = 800})
    require'cmp'.setup.buffer {
      sources = l_sources,
    }
  end,
}
-- }}}

-- default config: https://github.com/hrsh7th/nvim-cmp/blob/main/lua/cmp/config/default.lua
cmp.setup({
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-n>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,
    ['<C-y>'] = cmp.config.disable,
    ['<C-e>'] = cmp.config.disable,
    -- defaults only in command-line
    --['<Tab>'] = cmp.config.disable,
    --['<S-Tab>'] = cmp.config.disable,
    ['<C-Space>'] = cmp.mapping({
      i = select_next_or_complete,
      c = select_next_or_complete,
    }),
    ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
    ['<C-z>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
      s = cmp.mapping.close(),
    }),
    ['<CR>'] = cmp.mapping.confirm(),
    -- scroll only in docs
    ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c', 's' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c', 's' }),
  } ,

  sources = sources,

  formatting = {
    format = function(entry, vim_item)
      --print(vim.inspect(vim_item.kind) .. vim.inspect(entry.source.name))
      local new_kind_symbol = ({
        Text = "",
        Method = "ƒ",
        Function = "",
        Constructor = "",
        Field = "ﴲ",
        Variable = "",
        Class = "פּ",
        Interface = "",
        Module = "",
        Property = "",
        Unit = "",
        Value = "",
        Enum = "",
        Keyword = "",
        Snippet = "",
        Color = "",
        File = "",
        Reference = "",
        Folder = "",
        EnumMember = "",
        Constant = "",
        Struct = "",
        Event = "ﳅ",
        Operator = "",
        TypeParameter = "",
      })[vim_item.kind]
      if new_kind_symbol ~= nil then
        vim_item.kind = new_kind_symbol
      else
        vim_item.kind = ''
      end

      local new_source_symbol = ({
        luasnip = " ",
        nvim_lsp = " ",
        buffer = " ﬘",
        cmp_git = ' שׂ',
        calc = ' ',
        emoji = ' ﮚ',
        treesitter = ' ',
        latex_symbols = ' α',
        rg = ' ',
        nvim_lua = " ",
        spell =  ' 暈',
        cmdline = ' ',
        path = ' ',
      })[entry.source.name]
      if new_source_symbol ~= nil then
        vim_item.menu = new_source_symbol
      else
        vim_item.menu = "[" .. entry.source.name .. "]"
      end
      --print(vim.inspect(vim_item.kind) .. vim.inspect(entry.source.name))
      return vim_item
    end
  },
})

-- -- Use buffer source for `/`.
-- cmp.setup.cmdline('/', {
--   sources = {
--     { name = 'buffer' },
--   }
-- })

-- Use cmdline & path source for ':'.
cmp.setup.cmdline(':', {
  sources = cmp.config.sources({
    { name = 'path' },
    { name = 'cmdline' },
  })
})
