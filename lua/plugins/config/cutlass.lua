-- lua/plugins/config/cutlass.lua

local map = require('helper.key_map')
-- remap x to old dd (delete and copy)
map('n', 'x', 'd', 'noremap')
map('x', 'x', 'd', 'noremap')
map('n', 'xx', 'dd', 'noremap')
map('n', 'X', 'D', 'noremap')
