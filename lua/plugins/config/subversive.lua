-- lua/plugins/config/subversive.lua

local map = require('helper.key_map')

-- s for substitute
--nmap s <plug>(SubversiveSubstitute)
--nmap ss <plug>(SubversiveSubstituteLine)
--nmap S <plug>(SubversiveSubstituteToEndOfLine)

-- with prompt
map('n', 's' , '<plug>(SubversiveSubstituteRange)')
map('x', 's' , '<plug>(SubversiveSubstituteRange)')
map('n', 'ss', '<plug>(SubversiveSubstituteWordRange)')

-- with no prompt, use provided register (no register provide -> default register)
--  "a<leader>siwip
--  "_<leader>siwip for delete (black hole register)
map('n', '<Leader>s' , '<plug>(SubversiveSubstituteRangeNoPrompt)')
map('x', '<Leader>s' , '<plug>(SubversiveSubstituteRangeNoPrompt)')
map('n', '<Leader>ss', '<plug>(SubversiveSubstituteWordRangeNoPrompt)')

-- not used
--map('n', '<Leader>cs' , '<plug>(SubversiveSubstituteRangeConfirm)')
--map('x', '<Leader>cs' , '<plug>(SubversiveSubstituteRangeConfirm)')
--map('n', '<Leader>css', '<plug>(SubversiveSubstituteWordRangeConfirm)')
