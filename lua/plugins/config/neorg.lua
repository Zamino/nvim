-- lua/plugins/config/neorg.lua

require('neorg').setup {
  -- Tell Neorg what modules to load
  load = {
    ["core.defaults"] = {}, -- Load all the default modules
    ["core.norg.concealer"] = {}, -- Allows for use of icons
    ["core.norg.dirman"] = { -- Manage your directories with Neorg
      config = {
        workspaces = {
          --my_workspace = "~/neorg",
          kivi_notes = "/opt/kivitendo-erp/neorg",
        }
      }
    },
  },
  ["core.norg.completion"] = {
    config = {
      engine = "nvim-compe" -- We current support nvim-compe and nvim-cmp only
    }
  },
}

-- This sets the leader for all Neorg keybinds. It is separate from the regular <Leader>,
-- And allows you to shove every Neorg keybind under one "umbrella".
local neorg_leader = "<Leader>" -- You may also want to set this to <Leader>o for "organization"

-- Require the user callbacks module, which allows us to tap into the core of Neorg
local neorg_callbacks = require('neorg.callbacks')

-- Listen for the enable_keybinds event, which signals a "ready" state meaning we can bind keys.
-- This hook will be called several times, e.g. whenever the Neorg Mode changes or an event that
-- needs to reevaluate all the bound keys is invoked
neorg_callbacks.on_event("core.keybinds.events.enable_keybinds", function(_, keybinds)

  -- Map all the below keybinds only when the "norg" mode is active
  keybinds.map_event_to_mode("norg", {
    n = { -- Bind keys in normal mode
      -- Keys for managing TODO items and setting their states
      { neorg_leader .. "gtd", "core.norg.qol.todo_items.todo.task_done" },
      { neorg_leader .. "gtu", "core.norg.qol.todo_items.todo.task_undone" },
      { neorg_leader .. "gtp", "core.norg.qol.todo_items.todo.task_pending" },
      {"<C-Space>", "core.norg.qol.todo_items.todo.task_cycle" }
    },
  }, { silent = true, noremap = true })

end)
