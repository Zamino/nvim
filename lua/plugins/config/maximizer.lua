-- lua/plugins/config/maximizer.lua

local map = require('helper.key_map')

map('n', '<Leader>m', ':MaximizerToggle!<Enter>', 'noremap', 'silent')

-- default
map('n', '<F3>', ':MaximizerToggle!<Enter>'       , 'noremap', 'silent')
map('v', '<F3>', '<Esc>:MaximizerToggle!<Enter>gv', 'noremap', 'silent')
map('i', '<F3>', '<C-o>:MaximizerToggle!<Enter>'  , 'noremap', 'silent')
