-- lua/plugins/config/mason.lua

-- keymap on_attach {{{
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true , silent=true }
  buf_set_keymap('n', '<leader>gD'   ,
    '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', '<leader>gd'   ,
    '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', '<leader>K'    ,
    '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', '<leader>gi'   ,
    '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<leader><C-k>',
    '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<leader>wa'   ,
    '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<leader>wr'   ,
    '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<leader>wl'   ,
    '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<leader>D'    ,
    '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<leader>rn'   ,
    '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<leader>gr'   ,
    '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<leader>le'   ,
    '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
  buf_set_keymap('n', '<leader>la'   ,
    '<cmd>lua vim.lsp.buf.code_action({ filter = function(a) return a.isPreferred end , apply = true })<CR>' , opts)
  buf_set_keymap('n', '[d'           ,
    '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d'           ,
    '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<leader>q'    ,
    '<cmd>lua vim.diagnostic.set_loclist()<CR>', opts)

  --buf_set_keymap('n', '<leader>lf', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.document_formatting then
    buf_set_keymap("n", "<leader>lf", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
  elseif client.server_capabilities.document_range_formatting then
    buf_set_keymap("n", "<leader>lf", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end

  -- Set autocommands conditional on server_capabilities
  if client.server_capabilities.document_highlight then
    vim.api.nvim_exec([[
      hi LspReferenceRead cterm=bold ctermbg=red guibg=LightYellow
      hi LspReferenceText cterm=bold ctermbg=red guibg=LightYellow
      hi LspReferenceWrite cterm=bold ctermbg=red guibg=LightYellow
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]], false)
  end
end
-- }}}

-- for snippet completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities) -- for nvim-cmp
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    'documentation',
    'detail',
    'additionalTextEdits',
  }
}

-- for lua_ls {{{
local function get_runtime_path_for_lua()
  local runtime_path = vim.split(package.path, ';')
  table.insert(runtime_path, "lua/?.lua")
  table.insert(runtime_path, "lua/?/init.lua")
  return runtime_path
end
-- }}}

local server_setups = {
   -- latex {{{
   texlab = {
     filetypes = { "tex", "markdown", "bib" },
     on_attach = on_attach,
     capabilities = capabilities,
   },
   --[[
   Commands:
   - TexlabBuild: Build the current buffer
   - TexlabForward: Forward search from current position

   Default Values:
   cmd = { "texlab" }
   filetypes = { "tex", "bib" }
   root_dir = vim's starting directory
   settings = {
     bibtex = {
       formatting = {
         lineLength = 120
       }
     },
     latex = {
       build = {
         args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "%f" },
         executable = "latexmk",
         onSave = false
       },
       forwardSearch = {
         args = {},
         onSave = false
       },
       lint = {
         onChange = false
       }
     }
   }
   ]]
  -- }}}
  -- c/cpp {{{
  clangd = {
    on_attach = on_attach,
    capabilities = capabilities,
    filetypes = { "c", "cpp", "objc", "objcpp" },
  },
  --[[
    Commands:
    - ClangdSwitchSourceHeader: Switch between source/header

    Default Values:
      capabilities = default capabilities, with offsetEncoding utf-8
      cmd = { "clangd", "--background-index" }
      filetypes = { "c", "cpp", "objc", "objcpp" }
      on_init = function to handle changing offsetEncoding
      root_dir = root_pattern("compile_commands.json", "compile_flags.txt", ".git") or dirname
  ]]
  -- }}}
  -- html {{{
  html = {
    on_attach = on_attach,
    capabilities = capabilities,
    filetypes = { "html", "html.heex" },
  },
  --[[
    Default Values:
        cmd = { "html-languageserver", "--stdio" }
        filetypes = { "html" }
        init_options = {
          configurationSection = { "html", "css", "javascript" },
          embeddedLanguages = {
            css = true,
            javascript = true
          }
        }
        root_dir = function(fname)
              return root_pattern(fname) or vim.loop.os_homedir()
            end;
        settings = {}
  ]]
  -- }}}
  -- lua {{{
  lua_ls = {
    on_attach = on_attach,
    capabilities = capabilities,
    update_in_insert = false,
    --cmd = {"/home/tamino/.local/share/nvim/lspinstall/lua/./sumneko-lua-language-server"};
    settings = {
      Lua = {
        runtime = {
          -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
          version = 'LuaJIT',
          -- Setup your lua path
          path = get_runtime_path_for_lua(),
        },
        diagnostics = {
          -- Get the language server to recognize the `vim` global
          globals = {'vim', 'use'},
        },
        workspace = {
          -- Make the server aware of Neovim runtime files
          library = vim.api.nvim_get_runtime_file("", true),
          checkThirdParty = false,
        },
        -- Do not send telemetry data containing a randomized but unique identifier
        telemetry = {
          enable = false,
        },
      },
    },
  },
  -- }}}
  -- elixir {{{
  elixirls = {
    on_attach = on_attach,
    capabilities = capabilities,
    update_in_insert = false,
    filetypes = { "elixir", "eelixir", "po"},
  },
  -- }}}
  -- perl {{{
  perlpls = {
    cmd = { "pls" },
    filetypes = { "perl" },
    --root_dir = util.find_git_ancestor,
    on_attach = on_attach,
    capabilities = capabilities,
    update_in_insert = false,
    settings = {
      perl = {
        perlcritic = {
          enabled = false,
        },
        inc = {
          "/opt/kivitendo-erp/", -- for kivitendo
        },
        --syntax = {
          --  perl = "/home/tamino/.config/nvim/after/compiler/vimparse.pl -I/opt/kivitendo-erp/ -c % $*",
          --},
        },
      },
      single_file_support = true,
  },
  --[[
  Install: cpan PLS
  ]]--
  perlls = {
    cmd = { "perl", "-MPerl::LanguageServer", "-e", "Perl::LanguageServer::run", "--", "--port 13603", "--nostdio 0", "--version 2.1.0" },
    filetypes = { "perl" },
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
      perl = {
        fileFilter = { ".pm", ".pl" },
        ignoreDirs = ".git",
        perlCmd = "perl",
        perlInc = "/opt/kivitendo-erp/"
      },
    },
    single_file_mode = true,
  },
  --[[
  Install: cpan Perl::LanguageServer
  ]]--
  perlnavigator = {
    cmd = { "perlnavigator", "--stdio" },
    filetypes = { "perl" },
    on_attach = on_attach,
    capabilities = capabilities,
    update_in_insert = false,
    settings = {
      perlnavigator = {
        perlPath = 'perl',
        -- includePaths = {'/opt/kivitendo-erp/', "$workspaceFolder"},
        includePaths = {'/opt/kivitendo-erp/'},
        enableWarnings = true,
        -- perltidyProfile = '',
        -- perlcriticProfile = '',
        perlcriticEnabled = true,
      },
    },
  },
  -- }}}
}

local automatic_servers = {
  "clangd",
  "cssls",
  "elixirls",
  "html",
  "tsserver",
  "perlnavigator",
  "r_language_server",
  "sqlls",
  "lua_ls",
  "texlab",
}

local manual_servers = {
  -- "perlls", -- not working
  -- "perlpls",
}

local all_servers = {}
for _, server in pairs(automatic_servers) do
  table.insert(all_servers, server)
end
for _, server in pairs(manual_servers) do
  table.insert(all_servers, server)
end

require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = automatic_servers,
}

for _, server in pairs(all_servers) do
  local server_opts = server_setups[server]
  if not server_opts then
    server_opts = {
      on_attach = on_attach,
      capabilities = capabilities,
      update_in_insert = false,
    }
  end
  require('lspconfig')[server].setup(server_opts)
end
