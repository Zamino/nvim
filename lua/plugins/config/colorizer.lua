--[[
DEFAULT_OPTIONS = {
RGB      = true;         -- #RGB hex codes
RRGGBB   = true;         -- #RRGGBB hex codes
names    = true;         -- "Name" codes like Blue
RRGGBBAA = false;        -- #RRGGBBAA hex codes
rgb_fn   = false;        -- CSS rgb() and rgba() functions
hsl_fn   = false;        -- CSS hsl() and hsla() functions
css      = false;        -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
css_fn   = false;        -- Enable all CSS *functions*: rgb_fn, hsl_fn
-- Available modes: foreground, background
mode     = 'background'; -- Set the display mode.
}
]]--

vim.opt.termguicolors = true

-- Attach to certain Filetypes, add special configuration for `html`
-- Use `background` for everything else.
require 'colorizer'.setup {
  css = {
    css = true;
  }
}
