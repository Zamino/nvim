-- gruvbox.lua
-- https://github.com/morhetz/gruvbox/wiki/Configuration

vim.g.gruvbox_bold = 1
vim.g.gruvbox_italic = 1
vim.g.gruvbox_contrast_dark = 'hard'

vim.cmd('silent! colorscheme gruvbox')
