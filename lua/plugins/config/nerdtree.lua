-- plugins/config/nerdtree.lua

vim.g.NERDTreeBookmarksFile = '~/.config/nvim/plug-conf/nerdtree/NERDTreeBookmarks'

local map = require('helper.key_map')
map('n', '<C-n>', ':NERDTreeToggle<Enter>', 'noremap', 'silent') -- Open/Close Nerdtree
