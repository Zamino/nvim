-- treesitter.lua

-- for neorg
local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()
parser_configs.norg = {
  install_info = {
    url = "https://github.com/vhyrro/tree-sitter-norg",
    files = { "src/parser.c", "src/scanner.cc" },
    branch = "main"
  },
}

local has_ts_configs, ts_configs = pcall(require, 'nvim-treesitter.configs')
if has_ts_configs then
  ts_configs.setup {
    ensure_installed = { --{{{
      'bash',
      'c',
      'cpp',
      'css',
      'elixir',
      'erlang',
      'heex',
      'html',
      'javascript',
      'json',
      'lua',
      'norg',
      -- 'perl',
      'python',
      'toml',
    }, --}}}
    highlight = {
      enable = true,
      use_languagetree = false,
      disable = {
        --'elixir',
      },
    },
    indent = {
      enable = false,
      disable = {
      },
    },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = 'gnn',
        node_incremental = 'grn',
        scope_incremental = 'grc',
        node_decremental = 'grm'
      }
    },
    refactor = {
      smart_rename = {enable = true, keymaps = {smart_rename = "grr"}},
      highlight_definitions = {enable = true},
      -- highlight_current_scope = { enable = true },
    },
    textobjects = {
      select = {
        enable = true,
        keymaps = {
          ['iF'] = {
            python = '(function_definition) @function',
            cpp = '(function_definition) @function',
            c = '(function_definition) @function',
            java = '(method_declaration) @function'
          },
          -- or you use the queries from supported languages with textobjects.scm
          ['af'] = '@function.outer',
          ['if'] = '@function.inner',
          ['aC'] = '@class.outer',
          ['iC'] = '@class.inner',
          ['ac'] = '@conditional.outer',
          ['ic'] = '@conditional.inner',
          --['ae'] = '@block.outer',
          --['ie'] = '@block.inner',
          ['ab'] = '@block.outer',
          ['ib'] = '@block.inner',
          ['al'] = '@loop.outer',
          ['il'] = '@loop.inner',
          ['is'] = '@statement.inner',
          ['as'] = '@statement.outer',
          ['ad'] = '@comment.outer',
          ['am'] = '@call.outer',
          ['im'] = '@call.inner'
        }
      }
    }
  }
end
