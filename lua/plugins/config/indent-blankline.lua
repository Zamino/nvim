-- indent-blankline.lua

require("ibl").setup {
  scope = {
    show_start = false,
    show_end = false,
  },
}

local map = require('helper.key_map')
map('n', '<Leader>tib', ':IndentBlanklineToggle<Enter>', 'noremap', 'silent')
