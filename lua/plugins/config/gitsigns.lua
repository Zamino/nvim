-- lua/plugins/config/gitsigns.lua
local map = require('helper.key_map')

require('gitsigns').setup {
  signs = {
    add          = {hl = 'GitSignsAdd'   , text = '+', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
    change       = {hl = 'GitSignsChange', text = '│', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
    delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    changedelete = {hl = 'GitSignsChange', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
  },
  numhl = false,
  linehl = false,
  watch_gitdir = {
    interval = 1000
  },
  sign_priority = 6,
  update_debounce = 100,
  status_formatter = nil, -- Use default
  diff_opts = {
    internal = true,
  },
}

map('n', ']c', "&diff ? ']c' : '<cmd>lua require\"gitsigns\".next_hunk()<CR>'", 'noremap', 'expr')
map('n', '[c', "&diff ? '[c' : '<cmd>lua require\"gitsigns\".prev_hunk()<CR>'", 'noremap', 'expr')

map('n', '<leader>hs', '<cmd>lua require"gitsigns".stage_hunk()<CR>',      'noremap')
map('n', '<leader>hu', '<cmd>lua require"gitsigns".undo_stage_hunk()<CR>', 'noremap')
map('n', '<leader>hr', '<cmd>lua require"gitsigns".reset_hunk()<CR>',      'noremap')
map('n', '<leader>hR', '<cmd>lua require"gitsigns".reset_buffer()<CR>',    'noremap')
map('n', '<leader>hp', '<cmd>lua require"gitsigns".preview_hunk()<CR>',    'noremap')
map('n', '<leader>hb', '<cmd>lua require"gitsigns".blame_line()<CR>',      'noremap')

-- Text objects
map('o', 'ih', ':<C-U>lua require"gitsigns".text_object()<CR>', 'noremap')
map('x', 'ih', ':<C-U>lua require"gitsigns".text_object()<CR>', 'noremap')

