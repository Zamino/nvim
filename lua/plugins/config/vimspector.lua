-- lua/plugins/config/vimspector.lua

--fun GotoWindow(id)
--  call win_gotoid(a:id)
--  --neads Maximizer
--  MaximizerToggle
--endfun

--" Debugger remaps
--nnoremap <leader>dd :call vimspector#Launch()<CR>
--nnoremap <leader>dc :call GotoWindow(g:vimspector_session_windows.code)<CR>
--nnoremap <leader>dt :call GotoWindow(g:vimspector_session_windows.tagpage)<CR>
--nnoremap <leader>dv :call GotoWindow(g:vimspector_session_windows.variables)<CR>
--nnoremap <leader>dw :call GotoWindow(g:vimspector_session_windows.watches)<CR>
--nnoremap <leader>ds :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
--nnoremap <leader>do :call GotoWindow(g:vimspector_session_windows.output)<CR>
--nnoremap <leader>de :call vimspector#Reset()<CR>


local map = require('helper.key_map')
map('n', '<leader>dl', '<Plug>VimspectorStepInto')
map('n', '<leader>dj', '<Plug>VimspectorStepOver')
map('n', '<leader>dk', '<Plug>VimspectorStepOut')
map('n', '<leader>d_', '<Plug>VimspectorRestart')
map('n', '<leader>d<leader>', '<Plug>VimspectorContinue')

map('n', '<leader>drc', '<Plug>VimspectorRunToCursor')
map('n', '<leader>dbp', '<Plug>VimspectorToggleBreakpoint')
map('n', '<leader>dbc', '<Plug>VimspectorToggleConditionalBreakpoint')
map('n', '<leader>dBP', 'vimspector#ClearBreakpoints()', 'noremap', 'expr')

-- <Plug>VimspectorStop
-- <Plug>VimspectorPause
-- <Plug>VimspectorAddFunctionBreakpoint
