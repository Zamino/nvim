vim.g.EasyMotion_do_mapping = 0 -- Disable default mappings

vim.g.EasyMotion_smartcase = 1 -- Turn on case-insensitive feature

local map = require('helper.key_map')
-- Jump to anywhere you want with minimal keystrokes, with just one key binding.
-- `s{char}{label}`
map('n', '\\', '<Plug>(easymotion-overwin-f)')
-- or
-- `s{char}{char}{label}`
-- Need one more keystroke, but on average, it may be more comfortable.
map('n', '<Leader>\\', '<Plug>(easymotion-overwin-f2)')
-- or

-- JK motions: Line motions
--map <leader>J <Plug>(easymotion-j)
--map <leader>K <Plug>(easymotion-k)

-- use EasyMotion for search
--map  <leader><bar> <Plug>(easymotion-sn)
--omap <leader><bar> <Plug>(easymotion-tn)

-- These `n` & `N` mappings are options. You do not have to map `n` & `N` to EasyMotion.
-- Without these mappings, `n` & `N` works fine. (These mappings just provide
-- different highlight method and have some other features )
--map  <leader>n <Plug>(easymotion-next)
--map  <leader>N <Plug>(easymotion-prev)
