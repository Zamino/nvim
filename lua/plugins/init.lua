-- lua/plugins/init.lua

-- install plugin manager if needed {{{
local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
  execute 'packadd packer.nvim'
end
-- }}}

-- initialize packer
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
-- Packer can manage itself as an optional plugin
use {'wbthomason/packer.nvim', opt = true}
-- snippets
use {
  'L3MON4D3/LuaSnip',
  config = "require('plugins.config.lua_snip')", -- config/lua_snip.lua
}
-- completion
use {
  'hrsh7th/nvim-cmp',
  config = "require('plugins.config.cmp')", -- config/cmp.lua
  requires = { -- {{{
    { 'hrsh7th/cmp-nvim-lsp',
      opt = false,
    },
    { 'hrsh7th/cmp-nvim-lua',
      after = 'cmp-git',
    },
    { 'hrsh7th/cmp-buffer',
      after = 'cmp-git',
    },
    { 'hrsh7th/cmp-path',
      after = 'cmp-git',
    },
    { 'hrsh7th/cmp-cmdline',
      after = 'cmp-git',
    },
    { 'hrsh7th/cmp-calc',
      after = 'cmp-git',
    },
    { 'hrsh7th/cmp-emoji',
      after = 'cmp-git',
    },
    { 'ray-x/cmp-treesitter',
      after = 'cmp-git',
    },
    { 'f3fora/cmp-spell',
      after = 'cmp-git',
    },
    { 'lukas-reineke/cmp-rg',
      after = 'cmp-git',
    },
    { 'kdheepak/cmp-latex-symbols',
      after = 'cmp-git',
      ft = {'markdown', 'tex'},
    },
    { 'saadparwaiz1/cmp_luasnip',
      after = 'cmp-git',
    },
    {
      'petertriho/cmp-git',
      requires = "nvim-lua/plenary.nvim",
      config = "require('plugins.config.cmp_git')", -- config/cmp_git.lua
      after = 'nvim-cmp',
    },
  }, -- }}}
}
-- LSP / DAP / Linters / Formaters
use {
  "williamboman/mason.nvim",
  config = "require('plugins.config.mason')", -- config/mason.lua
  requires = {
    "williamboman/mason-lspconfig.nvim",
    'neovim/nvim-lspconfig',
    'hrsh7th/cmp-nvim-lsp',
  },
}
-- treesitter
use {
  'nvim-treesitter/nvim-treesitter',
  config = "require('plugins.config.treesitter')", -- config/treesitter.lua
  --run =   "vim.cmd('TSUpdate')",
}
-- fuzzy finder
use {
  'nvim-telescope/telescope.nvim',
  requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
  config = "require('plugins.config.telescope')", -- config/telescope.lua
}
-- git signs
use {
  'lewis6991/gitsigns.nvim',
  requires = {'nvim-lua/plenary.nvim'},
  config = "require('plugins.config.gitsigns')", -- config/gitsigns.lua
}
-- git integration
use {
  'tpope/vim-fugitive',
}
-- change surroundings '([{ and more
use {
  'tpope/vim-surround',
}
-- remap . to handle plugin commands
use {
  'tpope/vim-repeat',
}
use {
  "nvim-neorg/neorg",
  after = {"nvim-treesitter"},  -- you may also specify telescope
  requires = "nvim-lua/plenary.nvim",
  config = "require('plugins.config.neorg')", -- config/neorg.lua
  ft = {"norg", "norg.txt"},
  cmd = 'NeorgStart',
}
-- aligning text
use {
  'godlygeek/tabular',
}
-- don't copy by d dd D c s ...
use {
  'svermeulen/vim-cutlass',
  config = "require('plugins.config.cutlass')", -- config/cutlass.lua
}
-- substitute text
use {
  'svermeulen/vim-subversive',
  config = "require('plugins.config.subversive')", -- config/subversive.lua
}
-- creating tables in vim
use {
  'dhruvasagar/vim-table-mode',
}
-- debugging in vim
use {
  'puremourning/vimspector',
  config = "require('plugins.config.vimspector')", -- config/vimspector.lua
  opt = true,
  keys = {
    {'n', '<leader>d_'},
    {'n', '<leader>d<leader>'},
    {'n', '<leader>drc'},
    {'n', '<leader>dbp'},
    {'n', '<leader>dbc'},
  },
}
-- maximize vim splits
use {
  'szw/vim-maximizer',
  config = "require('plugins.config.maximizer')", -- config/maximizer.lua
  opt = true,
  keys = {
    {'n', '<Leader>m'},
  },
  cmd = 'MaximizerToggle',
}
-- dispay color codes in termial
use {
  'norcalli/nvim-colorizer.lua',
  config = "require('plugins.config.colorizer')", -- config/colorizer.lua
  ft = {'css'},
}
-- use file manager inside nvim
use {
  'is0n/fm-nvim',
  config = "require('plugins.config.fm_nvim')", -- config/fm_nvim.lua
}
-- show indentation lines
use {
  "lukas-reineke/indent-blankline.nvim",
  config = "require('plugins.config.indent-blankline')", -- config/indent-blankline.lua
}
-- chance vim to an ide !!needs nvim >= 0.3!!
-- needs nodejs, python and npm/yarn

--Plug 'neoclide/coc.nvim', {'branch': 'release'}
--source $HOME/.config/nvim/plug-conf/coc.vim

-- Other Programms -------------------------------------------------------------
-- use vim in firefox
use {
  'glacambre/firenvim',
  run = function() vim.fn['firenvim#install'](0) end ,
  config = "require('plugins.config.firenvim')", -- config/firenvim.lua
}

-- Languages -------------------------------------------------------------------
---- comments ------------------------------------------------------------------
use {
  'numToStr/Comment.nvim',
  config = "require('plugins.config.comment')", -- config/comment.lua
}
---- elixir --------------------------------------------------------------------
--use { -- only for the moment, until 5sec loading time in treesitter is fixed
--  'elixir-editors/vim-elixir',
--}
    -- Plug 'mhinz/vim-mix-format' " errorformat for mix
---- Markdown ------------------------------------------------------------------
-- Markdown preview in browser
use {
  'iamcco/markdown-preview.nvim',
  run = function() vim.fn['mkdp#util#install']() end ,
  config = "require('plugins.config.markdown-preview')", -- config/markdown-preview.lua
  opt = true,
  ft = {'markdown'},
}
-- Create Table of Content form Markdown files
use {
  'rderik/vim-markdown-toc',
  branch = 'add-anchors-to-headings/drc2r',
  config = "require('plugins.config.markdown-toc')", -- config/markdown-toc.lua
  opt = true,
  ft = {'markdown'},
}

-- Color scheme ----------------------------------------------------------------
use {
  'morhetz/gruvbox',
  config = "require('plugins.config.gruvbox')", -- config/gruvbox.lua
}

-- AI --------------------------------------------------------------------------
use {
  'zbirenbaum/copilot.lua',
  cmd = "Copilot",
  event = "InsertEnter",
  config = "require('plugins.config.copilot')", -- config/copilot.lua
}

end)
