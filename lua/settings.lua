-- settings.lua

local opt = vim.opt -- set
local autocmd = require('autocmd')

-- recovery
opt.swapfile = false
opt.dir = '/tmp' --swap dir
opt.writebackup = false
opt.backup = false
opt.hidden = true

-- window size / scrolloff
opt.winwidth=48
opt.winheight=5
opt.winminwidth=3
opt.winminheight=5
opt.scrolloff=5
opt.sidescrolloff=5
opt.sidescroll=1

-- statusline
opt.ruler = true
opt.laststatus = 2
opt.statusline =
'%<%F'  .. -- full file path
' %{exists("g:loaded_fugitive")?fugitive#statusline():"" }' .. -- git info
'%h'    .. -- help buffer flag [Hilfe] or [help]
'%m'    .. -- modified flag [+] if modified , [-] if modifiable is off
'%r'    .. -- readonlyflag [RO]
'%='    .. -- next section
'%-16.([%l,%c%V]%)' .. -- show couser position [{line number},{colum}-{virtualcolum}]
' [%{strlen(&fenc)?&fenc:"none"}]' .. -- encoding
' %P'      -- relative position in file

-- search
opt.hlsearch = true
opt.incsearch = true
opt.ignorecase = true
opt.smartcase = true

-- commands
opt.history = 1000
opt.showcmd = true

-- wildmenu
opt.wildmenu = true
opt.wildmode = 'longest,list,full'

-- whitespaces
opt.listchars = 'tab:»·,extends:›,precedes:‹,nbsp:·,trail:·'
opt.list = true

-- line numbers
opt.number = true
opt.relativenumber = true
opt.numberwidth = 4

-- folding
opt.foldenable = true
opt.foldlevelstart = 0
opt.foldnestmax = 10
opt.foldmarker = '{{{,}}}'
opt.foldmethod = 'marker'

-- style
opt.background = 'dark'
opt.cmdheight = 2
opt.signcolumn = 'yes'

-- complete
opt.complete = '.,w,b,u,t,kspell' -- for first opened file
opt.completeopt = 'menu,menuone,noselect'
opt.shortmess:prepend('c')

-- add path to current directory for :find
opt.path:prepend('$PWD/**')

-- line {{{
opt.wrap = true

-- line highlighting
opt.textwidth = 80
opt.colorcolumn = '+1' -- makes redraw slower
opt.cursorline = true  -- makes redraw slower

-- line format
-- formatoptions gets overwriten in ftplugin
vim.cmd('filetype plugin on') -- load ftplugin for overwrite
autocmd.FileType = { -- overwrite ftplugin
  '*',
  function()
    vim.opt_local.formatoptions='nrjq'
  end,
}
--[[
'n' recoginze numbered lists
'r' insert comment leader after <Enter> in Insert mode
'j' where it makes sense remove comment leader when joining lines
'q' format comments by typing qg - uses formatprg or formatexp
]]
opt.formatprg = ''
-- }}}

-- keys settings {{{
opt.timeout = false

-- backspace
opt.backspace = 'indent,eol,nostop'

-- tab
opt.softtabstop = 0
opt.tabstop = 2
opt.expandtab = true
opt.shiftwidth = 2
opt.shiftround = false
-- }}}

-- copy / paste {{{
opt.clipboard = 'unnamed,unnamedplus'
-- }}}

-- other
opt.encoding = 'utf8'
opt.showmatch = true

