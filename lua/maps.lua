-- maps.lua
local map = require('helper.key_map')

map('n', '<Space>', '')
vim.g.mapleader = ' ' -- map leader to space

-- reload current luafile
map('n', '<Leader>R', ':luafile %<Enter>', 'noremap')

-- enable mouse
vim.o.mouse = 'a'

-- easy buffer switching (crlt+direction to switch buffer)
map('n', '<C-J>', '<C-W><C-J>', 'noremap')
map('n', '<C-K>', '<C-W><C-K>', 'noremap')
map('n', '<C-L>', '<C-W><C-L>', 'noremap')
map('n', '<C-H>', '<C-W><C-H>', 'noremap')

-- fast save
map('n', '<C-S>', ':update<Enter>', 'noremap', 'silent')
map('n', '<Leader><C-S>', ':update!<Enter>', 'noremap', 'silent')
map('v', '<C-S>', '<Esc>:update<Enter>gv', 'noremap', 'silent')
map('i', '<C-S>', '<C-O>:update<Enter>', 'noremap', 'silent')
map('o', '<C-S>', '<Esc>:update<Enter>', 'noremap', 'silent')

-- fast close windows
map('n', '<C-C>', ':q<Enter>', 'noremap')
map('n', '<Leader><C-C>', ':q!<Enter>', 'noremap')

-- don't open ex mode
map('n', 'Q', '', 'noremap')

-- make ^j the same as <Enter>
map('i', '<C-j>', '<Enter>')

-- <Leader><Leader> to open and fold sections
map('n', '<Leader><Leader>', 'za', 'noremap')

-- Use ^<Space> to create new lines w/o entering insert mode
map('n', '<C-Space>', 'o<Esc>', 'noremap')

-- for replacing without copying in visual mode
map('v', '<Leader>p', '"_dP', 'noremap')

-- select last pasted text
map('n', 'gp', '`[v`]', 'noremap')

-- clear last search
map('n', '<Leader>/', '/XYZXYZ-clear-search<Enter>', 'noremap', 'silent')
-- turn off search highlight
--map('n', '<Leader>/', ':set hlsearch!<Enter>', 'noremap', 'silent')

-- use backspace with <C-l> in insert mode
--inoremap <C-l> <Del>

-- quickly open vim_wiki(W)
map('n', '<Leader>W', ':tabe ~/documents/wiki/vim_wiki.md<Enter>', 'noremap')
-- quickly open config(C)
map('n', '<Leader>C', ':tabe ~/.config/nvim/init.lua<Enter>', 'noremap')

-- turn on/off spellchecking
map('n', '<F6>', ':setlocal spell! spelllang=en_us<Enter>', 'noremap')
map('n', '<F7>', ':setlocal spell! spelllang=de_de<Enter>', 'noremap')

-- map :make to <Leader><Enter> ( <C-m> is the same as <Enter>)
map('n', '<Leader><Enter>', ':make<Enter>', 'noremap')

-- leave insert in terminal with <Leader><C-C>
map('t', '<Leader><C-C>', '<C-\\><C-N>', 'noremap')

-- ceate file under the cursor
map('n', '<Leader>GF', ':e <cfile><Enter>', 'noremap')


-- resize panes with  arrow-keys
map('n', '<Right>', ':vertical resize +5<Enter>', 'noremap', 'silent')
map('n', '<Left>',  ':vertical resize -5<Enter>', 'noremap', 'silent')
map('n', '<Up>',   ':resize -5<Enter>', 'noremap', 'silent')
map('n', '<Down>', ':resize +5<Enter>', 'noremap', 'silent')

-- il = in line
map('o', 'il', ':exec "normal! ^v$"<Enter>', 'noremap')
-- ie = in entire buffer
map('o', 'ie', ':exec "normal! ggVG"<Enter>', 'noremap')
-- iv = current viewable text in the buffer
map('o', 'iv', ':exec "normal! HVL"<Enter>', 'noremap')

-- tabs {{{
-- create new tab
map('n', 'tn', ':tabnew ', 'noremap')

-- move between tabs
map('n', 'tk', ':tabnext<Enter>' , 'noremap', 'silent')
map('n', 'tj', ':tabprev<Enter>' , 'noremap', 'silent')

map('n', 'th', ':tabfirst<Enter>', 'noremap', 'silent')
map('n', 'tl', ':tablast<Enter>' , 'noremap', 'silent')
-- move tabs them self
map('n', 'tK', ':+tabmove<Enter>', 'noremap', 'silent')
map('n', 'tJ', ':-tabmove<Enter>', 'noremap', 'silent')

map('n', 'tH', ':0tabmove<Enter>', 'noremap', 'silent')
map('n', 'tL', ':$tabmove<Enter>', 'noremap', 'silent')
-- }}}

-- buffer management {{{
map('n', '<Leader>be', ':buffers<Enter>:buffer<Space>', 'noremap')
map('n', '<Leader>bE', ':buffers!<Enter>:buffer<Space>', 'noremap')
map('n', '<Leader>bd', ':bdelete<Enter>', 'noremap')

map('n', '<Leader>bk', ':bnext<Enter>', 'noremap')
map('n', '<Leader>bj', ':bprevious<Enter>', 'noremap')

map('n', '<Leader>bh', ':bfirst<Enter>', 'noremap')
map('n', '<Leader>bl', ':blast<Enter>', 'noremap')
-- }}}

-- restart Apache/Http
-- put at the END in /etc/sudoers with visudo:
--[[
# Allow to restart http sever
tamino ALL=(ALL) NOPASSWD: /usr/bin/systemctl restart httpd.service
]]--
map('n', '<Leader>HR', ':!sudo systemctl restart httpd.service<Enter>', 'noremap')
